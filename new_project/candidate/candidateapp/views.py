from django.shortcuts import render
from rest_framework import viewsets
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from rest_framework.response import Response
import pandas as pd
from .models import ContactUs
from .serializers import ContactSerializer


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class ContactViewset(viewsets.ModelViewSet):
    serializer_class = ContactSerializer
    queryset = ContactUs.objects.all()


class AnalyticalView(viewsets.ModelViewSet):
    serializer_class = ContactSerializer
    queryset = ContactUs.objects.none()

    def list(self, request, *args, **kwargs):
        start_date = self.request.GET.get("start_date", '2000-01-01')
        end_date = self.request.GET.get("end_date", '2025-01-01')
        data = {}
        mydates = pd.date_range(start_date, end_date).tolist()
        list1 = []
        for i in mydates:
            data['date'] = str(i)
            data['count'] = ContactUs.objects.filter(created_at=i).count()
            list1.append({'date': data['date'], 'count': data['count']})
        return Response(json.loads(json.dumps(list1)))
