from django.urls import include, path
from rest_framework import routers

from .views import ContactViewset, AnalyticalView

router = routers.DefaultRouter()

router.register('contactus', ContactViewset, basename='contactus')
router.register('analytical', AnalyticalView, basename='analytical')

urlpatterns = [
    path('', include(router.urls))
]
